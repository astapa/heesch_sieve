RM := rm -vf

EXEC := heesch
DEPS := main hexgrid edgematch heesch isohedral sieve

VERBOSE ?= 0

ifeq ($(shell test $(VERBOSE) -gt 0; echo $$?),0)
	CDEFS := $(CDEFS) -DVERBOSE
endif

ifeq ($(shell test $(VERBOSE) -gt 1; echo $$?),0)
	CDEFS := $(CDEFS) -DVERBOSE2
endif

CFLAGS := $(CFLAGS) -Wall -Wextra $(CDEFS)

OBJDIR := build
SRCDIR := src

OBJDEPS := $(patsubst %,$(OBJDIR)/%.o,$(DEPS))
ODEPS_A := $(OBJDEPS:.o=_alter.o)

all: CFLAGS := $(CFLAGS) -O3
all: $(EXEC)

debug: CFLAGS := $(CFLAGS) -g
debug: clean $(EXEC)

alter: CFLAGS := $(CFLAGS) -DALTER
alter: $(EXEC)_alter

clean:
	@$(RM) $(EXEC) $(EXEC)_alter
	@$(RM) $(OBJDEPS) $(ODEPS_A)
	@find -name '*_alter.c' -type l -delete

.PHONY: all debug alter clean

$(SRCDIR)/%_alter.c: $(SRCDIR)/%.c
	@echo generating link to $@
	@ln -vs $(notdir $<) $@

$(EXEC): $(OBJDIR) $(OBJDEPS)
	@echo linking $@
	@$(CC) -o $@ $(OBJDEPS) $(CFLAGS)

$(EXEC)_alter: $(OBJDIR) $(ODEPS_A)
	@echo linking $@
	@$(CC) -o $@ $(ODEPS_A) $(CFLAGS)

$(OBJDIR):
	@mkdir -v $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@echo $(CC) $<
	@$(CC) -c -o $@ $< $(CFLAGS)
