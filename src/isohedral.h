#ifndef _ISOHEDRAL_H
#define _ISOHEDRAL_H

#include <stdint.h>

int check_isohedral(uint64_t rule);

const char *iso_code(int index);

#endif /* _ISOHEDRAL_H */
