#ifndef HEXGRID_H
#define HEXGRID_H

#include <stdint.h>
#include <stddef.h>

#define HEX_X(coords) ((int16_t) (coords & 0xFFFF))
#define HEX_Y(coords) ((int16_t) (coords >> 16))

#define PACK_COORDS(x,y) ((((uint32_t) y) << 16) | ((uint16_t) x))

/* get index of cell at coordinates x,y */
size_t hex_index(int16_t x, int16_t y);

/* get coordinates of cell at index i, in the form x + (y << 16) */
uint32_t hex_coords(size_t i);

#endif /* HEXGRID_H */
