#ifndef EDGEMATCH_H
#define EDGEMATCH_H

#include <stdint.h>
#include <stddef.h>

#define MAX_XFORM 0xC
#define NULL_XFORM -1
#define FULL_RULE 0x3FFFFFFFFFFL

typedef int xform_t;
typedef uint64_t rule_t;

/* edge matching rules are stored in this order:
 * 55  54 53  52 51  50 44  43 42  41 40  33 32  31 30  22 21  20 11  10 00
 */
enum coupling_masks
{
	ZERO_COUPLING = 0x000C03030CFL,
	ONE_COUPLING  = 0x00300C0C33CL,
	TWO_COUPLING  = 0x00C03030FC0L,
	THREE_COUPLING= 0x0300C0FF000L,
	FOUR_COUPLING = 0x0C03FF00000L,
	FIVE_COUPLING = 0x3FFC0000000L
};

/* 
 * transform combines rotation and reflection
 * lowest bit is reflection bit
 * the higher bits are the number of edges to rotate (shifted up)
 * input is edge number relative to grid direction
 * output is edge number on untranslated polygon
 * rotation is clockwise for non-reflections, ccw for reflections
 */
#define TRANSFORM(edge, xform) (((((xform) & 0x1) ? (6 - (edge)) : (edge)) + 6 - ((xform) >> 1)) % 6)

#define ALL_MATCH(rule)							\
	((rule & ZERO_COUPLING)						\
	&& (rule & ONE_COUPLING)					\
	&& (rule & TWO_COUPLING)					\
	&& (rule & THREE_COUPLING)					\
	&& (rule & FOUR_COUPLING)					\
	&& (rule & FIVE_COUPLING))

#define SHIFT(hi,lo) ((hi) * ((hi) + 1) + (2 * (lo)))

/* gets matching rules for each side separately */
void extract_links(uint16_t links[], uint64_t rule);

/* rebuilds rule given bu array of links, rotated clockwise by shift */
uint64_t rebuild_rule(uint16_t links[], int shift);

/* reflects rule given by array of links; reflect is done in-place */
void flip_links(uint16_t links[]);

/* finds the largest rule equivalent to given rule */
uint64_t max_equiv(uint64_t rule);

long edge_match(uint64_t rule, int s1, int s2, int flip);

#endif /* EDGEMATCH_H */
