#ifndef _SIEVE_H
#define _SIEVE_H

#include <stdio.h>
#include <stdint.h>

int analyze_rule(uint64_t rule);

void sift_by_links(unsigned int max_links, FILE *outfile);
void sift_partial(uint8_t links[], unsigned int set, unsigned int sift, FILE *outfile);

int advance_links(uint8_t links[], unsigned int nlinks, unsigned int set, unsigned int leave);
void seed_links(uint8_t links[], unsigned int set, unsigned int add);

#endif /* _SIEVE_H */
