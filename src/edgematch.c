#include "edgematch.h"

#include <stdint.h>
#include <stddef.h>

#include "common.h"

#define ROLL(x,r) ((((x) << (2 * (r))) & 0xFFF) | (((x) & (0xFFFF << (12 - (2 * (r))))) >> (12 - (2 * (r)))))
#define FLIP(x) (((x) & 0xC3) + (((x) & 0xC) << 8) + (((x) & 0x30) << 4) + (((x) & 0x300) >> 4) + (((x) & 0xC00) >> 8))

void extract_links(uint16_t buffer[], uint64_t rule)
{
	uint16_t link, links;
	int i, j, shift;
	for (i = 0; i < 6; ++i)
	{
		links = 0;
		for (j = 0; j < i; ++j)
		{
			shift = SHIFT(i,j);
			link = (rule & (0x3L << shift)) >> (shift - 2 * j);
			links += link;
		}
		for (j = i; j < 6; ++j)
		{
			shift = SHIFT(j,i);
			link = (rule & (0x3L << shift)) >> (shift - 2 * j);
			links += link;
		}
		buffer[i] = links;
	}
}

uint64_t rebuild_rule(uint16_t links[], int shift)
{
	/* let's rotate clockwise */
	int eff_shift = (6 + shift) % 6;
	
	/* 5 coupling */
	uint64_t ans = ((uint64_t) ROLL(links[(11 - eff_shift) % 6], eff_shift)) << 30;

	/* 4 coupling, not including 4-5 */
	ans |= ((uint64_t) (ROLL(links[(10 - eff_shift) % 6], eff_shift) & 0x3FF)) << 20;

	/* 3 coupling, not including 3-(4|5) */
	ans |= ((uint64_t) (ROLL(links[(9 - eff_shift) % 6], eff_shift) & 0xFF)) << 12;

	/* 2 coupling */
	ans |= ((uint64_t) (ROLL(links[(8 - eff_shift) % 6], eff_shift) & 0x3F)) << 6;

	/* 1 coupling */
	ans |= ((uint64_t) (ROLL(links[(7 - eff_shift) % 6], eff_shift) & 0xF)) << 2;

	/* 0-0 coupling */
	ans |= ((uint64_t) (ROLL(links[(6 - eff_shift) % 6], eff_shift) & 0x3));

	return ans;
}

void flip_links(uint16_t links[])
{
	uint16_t swap;
	links[0] = FLIP(links[0]);
	links[3] = FLIP(links[3]);

	swap = FLIP(links[1]);
	links[1] = FLIP(links[5]);
	links[5] = swap;

	swap = FLIP(links[2]);
	links[2] = FLIP(links[4]);
	links[4] = swap;
}

uint64_t max_equiv(uint64_t rule)
{
	uint16_t links[6];
	uint64_t max, cur;
	int i;

	max = rule;
	extract_links(links, rule);
	for (i = 1; i < 6; ++i)
	{
		cur = rebuild_rule(links, i);
		if (cur > max)
		{
			max = cur;
		}
	}

	flip_links(links);
	for (i = 0; i < 6; ++i)
	{
		cur = rebuild_rule(links, i);
		if (cur > max)
		{
			max = cur;
		}
	}
	return max;
}

long edge_match(uint64_t rule, int s1, int s2, int flip)
{
	if (s1 < 0 || s2 < 0) /* blank spot on the grid */
	{
		return 1;
	}
	
	unsigned int hi, lo, shift;
	hi = MAX(s1,s2);
	lo = MIN(s1,s2);
	shift = SHIFT(hi, lo) + flip;
	return (0x1L << shift) & rule;
}
