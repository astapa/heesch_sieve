#ifndef HEESCH_H
#define HEESCH_H

#include <stdint.h>

int is_max(uint64_t rule);

/* returns non-zero if rule will tile the plane isomorphically */
int check_isomorhpic(uint64_t rule);

/* attempts to tile the plane, returns heesch number */
int check_tile(uint64_t rule);

#endif /* HEESCH_H */
