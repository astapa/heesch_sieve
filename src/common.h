#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>

#define MAX(x,y) ((x > y) ? x : y)
#define MIN(x,y) ((x < y) ? x : y)

#endif /* COMMON_H */
