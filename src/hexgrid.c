#include "hexgrid.h"

#include <stdint.h>

#include "coord_table.h"
#include "common.h"

#define SIGN(x) ((x) & 0x8000)
#define ABS(x) (SIGN((x)) ? -(x) : (x))

uint32_t hex_coords(size_t i)
{
	return COORDS[i];
}

size_t hex_index(int16_t x, int16_t y)
{
	if (!x && !y)
	{
		return 0;
	}
	
	int corona;
	if (SIGN(x) ^ SIGN(y))
	{
		corona = MAX(ABS(x),ABS(y));
	} else
	{
		corona = ABS(x + y);
	}

	size_t corona_first = 3 * corona * (corona - 1) + 1;
	if (y == corona)
	{
		return corona_first - x;
	} else if (x == -corona)
	{
		return corona_first + (2 * corona) - y;
	} else if (x < 0 && y <= 0)
	{
		return corona_first + (2 * corona) - y;
	} else if (y == -corona)
	{
		return corona_first + (3 * corona) + x;
	} else if (x == corona)
	{
		return corona_first + (5 * corona) + y;
	}
	
	return corona_first + (5 * corona) + y;
}
