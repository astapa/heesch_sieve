#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include <mpi.h>

#include "sieve.h"

#ifndef N_DELEGATE
#define N_DELEGATE 5
#endif

enum
{
	MSG_SLAVE_READY,
	MSG_JOB_NSET,
	MSG_JOB_NSIFT,
	MSG_JOB_LINKS
};

#define MAX_LINKS 6

void master_process(unsigned int max_links, unsigned int delegate, int nslaves);
void slave_process(int procnum);

void delegate_sift(uint8_t links[], unsigned int set, unsigned int sift, int procnum);
void terminate(int procnum);

int main(int argc, char *argv[])
{
	// 10  00 10  00 00  10 01  10 00  00 01  00 10  00 10  00 11  01 10  00 00
//	uint64_t rule = 0x22098122360L;
	//sift_by_links(argc == 1 ? 5 : atoi(argv[1]), stdout);
	int result = MPI_Init(&argc, &argv);
	
	int total, procnum;
	MPI_Comm_size(MPI_COMM_WORLD, &total);
	MPI_Comm_rank(MPI_COMM_WORLD, &procnum);
	if (procnum)
	{
		slave_process(procnum);
	} else
	{
		master_process(MAX_LINKS, N_DELEGATE, total-1);
	}
	MPI_Finalize();
}

void master_process(unsigned int max_links, unsigned int delegate, int nslaves)
{
	MPI_Status status;
	
	uint8_t links[21];
	unsigned int nlinks = 3;
	int re_seed = 1;

	int procnum;

	while (nlinks <= delegate)
	{
		MPI_Recv(&procnum, 1, MPI_INT, MPI_ANY_SOURCE, MSG_SLAVE_READY, MPI_COMM_WORLD, &status);
		delegate_sift(links, 0, nlinks++, procnum);
	}

	while (nlinks <= max_links)
	{
		if (re_seed)
		{
			seed_links(links, 0, nlinks - delegate);
		}

		MPI_Recv(&procnum, 1, MPI_INT, MPI_ANY_SOURCE, MSG_SLAVE_READY, MPI_COMM_WORLD, &status);
		delegate_sift(links, nlinks - delegate, delegate, procnum);

		if ((re_seed = advance_links(links, nlinks - delegate, 0, delegate)))
		{
			++nlinks;
		}
	}

	while (nslaves--)
	{
		MPI_Recv(&procnum, 1, MPI_INT, MPI_ANY_SOURCE, MSG_SLAVE_READY, MPI_COMM_WORLD, &status);
		terminate(procnum);
	}
	return;
}

void slave_process(int procnum)
{
	MPI_Status status;
	unsigned int set, sift;
	uint8_t links[21];
	char filename[64];
	int batch = 1;
	
	for (;;)
	{
		MPI_Send(&procnum, 1, MPI_INT, 0, MSG_SLAVE_READY, MPI_COMM_WORLD);
		MPI_Recv(&set, 1, MPI_UNSIGNED, 0, MSG_JOB_NSET, MPI_COMM_WORLD, &status);
		MPI_Recv(&sift, 1, MPI_UNSIGNED, 0, MSG_JOB_NSIFT, MPI_COMM_WORLD, &status);
		if (set == 0 && sift == 0) /* end task */
		{
			return;
		}
		MPI_Recv(&links, set, MPI_UNSIGNED_CHAR, 0, MSG_JOB_LINKS, MPI_COMM_WORLD);
		sprintf(filename, "proc%dbatch%d", procnum, batch++);
		FILE *outfile = fopen(filename, "w");
		sift_partial(links, set, sift, outfile);
		fclose(outfile);
	}
}

void delegate_sift(uint8_t links[], unsigned int set, unsigned int sift, int procnum)
{
	MPI_Send(&set, 1, MPI_UNSIGNED, procnum, MSG_JOB_NSET, MPI_COMM_WORLD);
	MPI_Send(&sift, 1, MPI_UNSIGNED, procnum, MSG_JOB_NSIFT, MPI_COMM_WORLD);
	MPI_Send(links, set, MPI_UNSIGNED_CHAR, procnum, MSG_JOB_LINKS, MPI_COMM_WORLD);
}

void terminate(int procnum)
{
	unsigned int send = 0;
	MPI_Send(&send, 1, MPI_UNSIGNED, procnum, MSG_JOB_NSET, MPI_COMM_WORLD);
	MPI_Send(&send, 1, MPI_UNSIGNED, procnum, MSG_JOB_NSIFT, MPI_COMM_WORLD);
}
