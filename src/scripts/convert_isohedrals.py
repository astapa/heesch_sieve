#!/usr/bin/python3

# script to convert isohedral tilings
# from incidence symbols to generalized edge-matching rules

def encode_rule(rule):
    count = 0
    ans = 0
    for row in rule:
        for x in row:
            ans += (x << count)
            count += 2
    return ans

def parse_iso(iso):
    tilesym, adjsym = iso.split(';')

    sides = []
    labels = []
    i = 0
    while i < len(tilesym):
        l = tilesym[i]
        aux = 0 # 1 for cw, -1 for ccw, 0 for unlabeled
        if (i + 1) < len(tilesym):
            n = tilesym[i+1]
            if n == '+' or n == '-':
                i += 1
                aux = 1 if n == '+' else -1
        i += 1
        sides.append((l,aux))
        if l not in labels:
            labels.append(l)

    i = 0
    j = 0
    adj = {}
    while i < len(adjsym):
        l = labels[j]
        l2 = adjsym[i]
        aux = 3 # 1 for unflipped, 2 for flipped, 3 for both
        if (i + 1) < len(adjsym):
            n = adjsym[i+1]
            if n == '+' or n == '-':
                i += 1
                aux = 1 if n == '+' else 2
        i += 1
        j += 1
        adj[l] = (l2,aux)

    rule = []
    for i in range(len(sides)):
        row = []
        l, dir = sides[i]
        target, t = adj[l]
        for j in range(i+1):
            l2, dir2 = sides[j]
            if l2 == target:
                if t == 2:
                    if dir + dir2 == 0:
                        row.append(1)
                    else:
                        row.append(2)
                elif t == 1:
                    if dir + dir2 == 0:
                        row.append(2)
                    else:
                        row.append(1)
                elif t == 3:
                    row.append(3)
                else:
                    row.append(0)
            else:
                row.append(0)
        rule.append(row)

    return rule

def roll_rule(rule):
    nsides = len(rule)
    ans = []
    for i in range(nsides):
        row = []
        for j in range(i+1):
            s1 = (i + nsides - 1) % nsides
            s2 = (j + nsides - 1) % nsides
            row.append(rule[max(s1,s2)][min(s1,s2)])
        ans.append(row)
    return ans

def flip_rule(rule):
    nsides = len(rule)
    ans = []
    for i in range(nsides):
        row = []
        for j in range(i+1):
            s1 = (nsides - i) % nsides
            s2 = (nsides - j) % nsides
            row.append(rule[max(s1,s2)][min(s1,s2)])
        ans.append(row)
    return ans

if __name__ == '__main__':
    inf = open("isostrings", 'r')
    n = 1
    for line in inf:
        line = line.strip()
        rule = parse_iso(line)
        rule_code = encode_rule(rule)

        variants = [rule_code]
        nextr = roll_rule(rule)
        next = encode_rule(nextr)
        while next not in variants:
            variants.append(next)
            nextr = roll_rule(nextr)
            next = encode_rule(nextr)

        nextr = flip_rule(rule)
        next = encode_rule(nextr)
        while next not in variants:
            variants.append(next)
            nextr = roll_rule(nextr)
            next = encode_rule(nextr)

        print("// IH" + str(n))
        for v in variants:
            print(hex(v) + "L,")
        n += 1
    inf.close()
