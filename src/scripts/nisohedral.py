#!/usr/bin/python3

# script to find n-isohedral tilings of hexagons
# yes, it's a mess
# no, I'm probably not going to clean it up

angles = [1, 1, 2, 1, 1, 1, 1, 2, 1, 1]
verts  = list(range(10))

def seed_partition(npart, verts):
    ans = [-1] * npart
    for i in range(1,npart):
        ans[i] = verts[-npart + i]
    return ans

def advance_partition(part, max_vert):
    l = len(part) - 1
    for i in range(l, -1, -1):
        room = l - i
        if part[i] < (max_vert - room):
            part[i] += 1
            for j in range(i+1, l+1):
                part[j] = part[i] + (j-i)
            return True

    return False

def get_sides(part, verts):
    ans = []
    for i in range(len(part)-1):
        ans.append(verts[part[i]:part[i+1]+1])
    ans.append(verts[part[-1]:] + verts[:part[0]+1])
    return ans

def sides_couple(side1, side2, angles, forward=True):
    l = len(side1)-1
    for i in range(1, l):
        opp = l-i if forward else i
        if angles[side1[i]] + angles[side2[opp]] != 3:
            return False

    if not forward:
        return (angles[side1[0]] + angles[side2[0]] <= 3) and (angles[side1[-1]] + angles[side2[-1]] <= 3)
    return (angles[side1[0]] + angles[side2[-1]] <= 3) and (angles[side1[-1]] + angles[side2[0]] <= 3)

def get_coupling(side1, side2, angles):
    l = len(side1)
    if len(side2) != l:
        return 0

    ans = 0
    if sides_couple(side1, side2, angles):
        ans += 1
    if sides_couple(side1, side2, angles, False):
        ans += 2
    return ans

def rule_valid(rule):
    nsides = len(rule)
    for i in range(nsides):
        side_couples = False
        for j in range(nsides):
            if rule[max(i,j)][min(i,j)] != 0:
                side_couples = True
        if not side_couples:
            return False
    return True

def make_rule(sides, angles):
    nsides = len(sides)
    rule = []
    for i in range(nsides):
        rule.append([0] * (i+1))

    for i in range(nsides):
        for j in range(i+1):
            rule[i][j] = get_coupling(sides[i], sides[j], angles)
    return rule

def encode_rule(rule):
    count = 0
    ans = 0
    for row in rule:
        for x in row:
            ans += (x << count)
            count += 2
    return ans

def parse_isohedrals(lines):
    ans = []
    current = None
    for l in lines:
        if len(l) == 0:
            continue
        if l[0] == '/': # start of a new tiling type
            ans.append(current)
            current = []
        else:
            current.append(int(l[:-2], 16))
    ans.append(current)
    return ans

def num_sides_enc(rule):
    i = 6
    while True:
        mask = ((2 ** (2 * i)) - 1) << (i * (i - 1))
        if rule & mask != 0:
            return i
        i -= 1

def match_isohedral(rule, isos):
    ans = []
    for i in range(1, len(isos)):
        for r in isos[i]:
            if (rule & r == r) and (num_sides_enc(rule) == num_sides_enc(r)):
                ans.append(i)
                break
    return ans

def main():
    infl = open("converted")
    iso_raw = infl.read()
    infl.close()
    lines = iso_raw.split('\n')
    isos = parse_isohedrals(lines)
    for npart in range(3, 7):
        part = seed_partition(npart, verts)
        while advance_partition(part, verts[-1]):
            sides = get_sides(part, verts)
            rule = make_rule(sides, angles)
            if rule_valid(rule):
                r = encode_rule(rule)
                match = match_isohedral(r, isos)
                if len(match) > 0:
                    print("IH" + str(match) + ':', part)

if __name__ == '__main__':
    main()
