#include "isohedral.h"

#include <stddef.h>
#include <stdint.h>

#include "isohedral_data.h"

int check_isohedral(uint64_t rule)
{
	size_t i;
	for (i = 0; i < NUM_HEX_ISOS; ++i)
	{
		if ((rule & ISO_RULES[i]) == ISO_RULES[i])
		{
			return i;
		}
	}
	return -1;
}

const char *iso_code(int index)
{
	return ISO_NAMES[index];
}
