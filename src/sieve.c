#include "sieve.h"

#include <stdio.h>
#include <stdint.h>

#include "heesch.h"
#include "isohedral.h"
#include "edgematch.h"

int advance_one_link(uint8_t links[], unsigned int nlinks, size_t i, uint8_t lim);
uint64_t gen_rule(uint8_t links[], unsigned int nlinks);

int analyze_rule(uint64_t rule)
{
	int iso;
	if ((iso = check_isohedral(rule)) >= 0)
	{
		return -(iso + 1);
	}
   	return check_tile(rule);
}

void sift_by_links(unsigned int maxlinks, FILE *outfile)
{
	uint8_t links[21];
	unsigned int nlinks = 3;
	uint64_t rule;
	int result;

	seed_links(links, 0, nlinks);
	do
	{
		rule = gen_rule(links, nlinks);
		if (ALL_MATCH(rule))
		{
#ifdef VERBOSE2
			fprintf(stderr, "\tchecking 0x%011lX...", rule);
#endif
			if (is_max(rule))
			{
				fprintf(outfile, "0x%011lX ", rule);
				if ((result = analyze_rule(rule)) < 0) /* isohedral */
				{
					fputs(iso_code((-result) - 1), outfile);
				} else /* heesch */
				{
					fprintf(outfile, "%d\n", result);
				}
			}
#ifdef VERBOSE2
			fputs("done\n", stderr);
#endif
		}

		if (advance_links(links, nlinks, 0, 0)) /* maximum rule reached */
		{
			seed_links(links, 0, ++nlinks);
		}
	} while (nlinks <= maxlinks);
}

void sift_partial(uint8_t links[], unsigned int set, unsigned int sift, FILE *outfile)
{
	unsigned int nlinks = set + sift;
	uint64_t rule;
	int result;

	seed_links(links, set, sift);
	do
	{
		rule = gen_rule(links, nlinks);
		if (ALL_MATCH(rule))
		{
#ifdef VERBOSE2
			fprintf(stderr, "\tchecking 0x%011lX...", rule);
#endif
			if (is_max(rule))
			{
				fprintf(outfile, "0x%011lX ", rule);
				if ((result = analyze_rule(rule)) < 0) /* isohedral */
				{
					fputs(iso_code((-result) - 1), outfile);
				} else /* heesch */
				{
					fprintf(outfile, "%d\n", result);
				}
			}
#ifdef VERBOSE2
			fputs("done\n", stderr);
#endif
		}

		if (advance_links(links, nlinks, set, 0)) /* maximum reached */
		{
			return;
		}
	} while (1);
}

/* links are stored in the following format:
 * the lowest 2 bits are the actual link type (0, 1, 2, or 3)
 * the rest are the index where the link is located (shifted up, of course)
 */
int advance_links(uint8_t links[], unsigned int nlinks, unsigned int set, unsigned int leave)
{
	size_t i;
	uint8_t limit;
	for (i = nlinks - 1; i > set; --i)
	{
		limit = (nlinks - i - 1 + leave) * 4;
		if (advance_one_link(links, nlinks, i, limit))
		{
			return 0;
		}
	}

	/* stop the search if we advance the last link past edge 5 */
	limit = set ? ((nlinks - set - 1 + leave) * 4) : 60;
	if (advance_one_link(links, nlinks, set, limit))
	{
		return 0;
	}
	return 1;
}

int advance_one_link(uint8_t links[], unsigned int nlinks, size_t i, uint8_t limit)
{
	uint8_t new_link = links[i] - 1;
	if (new_link > limit) /* we can advance this link */
	{
		if ((new_link & 0x3) == 0) /* we just got unlinked */
		{
			--new_link;
		}

		links[i] = new_link;
		new_link |= 3;
		/* fill in all links after this one */
		for (i = i + 1; i < nlinks; ++i)
		{
			new_link -= 4;
			links[i] = new_link;
		}
		return 1;
	}
	/* can't advance */
	return 0;
}

void seed_links(uint8_t links[], unsigned int set, unsigned int add)
{
	size_t i;
	uint8_t link;
	if (set)
	{
		link = (links[set-1] & 0xFC) - 1;
	} else
	{
		link = 0x53;
	}

	for (i = set; i < set + add; ++i)
	{
		links[i] = link;
		link -= 0x4;
	}
}

uint64_t gen_rule(uint8_t links[], unsigned int nlinks)
{
	size_t i;
	uint64_t rule = 0x0L;
	for (i = 0; i < nlinks; ++i)
	{
		uint8_t link = links[i];
		int shift = (link >> 2) * 2;
		uint64_t type = link & 0x3;
		rule += (type << shift);
	}
	return rule;
}
