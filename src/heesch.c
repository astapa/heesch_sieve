#include "heesch.h"

#include <stdint.h>
#include <stddef.h>
#include <time.h>

#include "common.h"
#include "hexgrid.h"
#include "edgematch.h"

#define CORONA_LAST(x) (3 * (x) * ((x) + 1))

#ifndef GRID_SIZE
#define GRID_SIZE 16
#endif

#ifndef TILE_TIMEOUT
#define TILE_TIMEOUT 30
#endif

#define GRID_MAX (CORONA_LAST(GRID_SIZE) + 1)

static int grid[GRID_MAX];

int try_place(int *grid, uint64_t rule, size_t i, int orient);

#define TEST_MAX(expr)    \
	test = (expr);        \
	if (test > reference) \
	{                     \
		return 0;         \
	}

int is_max(uint64_t rule)
{
	uint64_t reference, test;
	reference = rule & 0x3FFC0000000L;

	/* rotate by 1 */
	TEST_MAX(((rule & 0x0003FF00000L) << 12) +
			 ((rule & 0x0C000000000L) >> 8));

	/* rotate by 2 */
	TEST_MAX(((rule & 0x000000FF000L) << 22) +
			 ((rule & 0x03000000000L) >> 6)  +
			 ((rule & 0x0000C000000L) << 4));

	/* rotate by 3 */
	TEST_MAX(((rule & 0x00000000FC0L) << 30) +
			 ((rule & 0x00C00000000L))       +
			 ((rule & 0x00003000000L) << 8)  +
			 ((rule & 0x00000030000L) << 14));

	/* rotate by 4 */
	TEST_MAX(((rule & 0x0000000003CL) << 36) +
			 ((rule & 0x00300000000L) << 4)  +
			 ((rule & 0x00000C00000L) << 12) +
			 ((rule & 0x0000000C000L) << 18) +
			 ((rule & 0x00000000300L) << 22));

	/* rotate by 5 */
	TEST_MAX(((rule & 0x00000000003L) << 40) +
			 ((rule & 0x000C0000000L) << 8)  +
			 ((rule & 0x00000300000L) << 16) +
			 ((rule & 0x00000003000L) << 22) +
			 ((rule & 0x000000000C0L) << 26) +
			 ((rule & 0x0000000000CL) << 28));

	/* flip */
	TEST_MAX(((rule & 0x00000000030L) << 36) +
			 ((rule & 0x00000000300L) << 30) +
			 ((rule & 0x0000000C000L) << 22) +
			 ((rule & 0x00000C00000L) << 12) +
			 ((rule & 0x00300000000L))       +
			 ((rule & 0x0000000000CL) << 28));

	/* flip + rotate 1 */
	TEST_MAX(((rule & 0x00000000C00L) << 30) +
			 ((rule & 0x00000030000L) << 22) +
			 ((rule & 0x00003000000L) << 12) +
			 ((rule & 0x00C00000000L))       +
			 ((rule & 0x000000000C0L) << 26) +
			 ((rule & 0x00000000300L) << 22));

	/* flip + rotate 2 */
	TEST_MAX(((rule & 0x000000C0000L) << 22) +
			 ((rule & 0x0000C000000L) << 12) +
			 ((rule & 0x03000000000L))       +
			 ((rule & 0x00000003000L) << 22) +
			 ((rule & 0x0000000C000L) << 18) +
			 ((rule & 0x00000030000L) << 14));
	
	/* flip + rotate 3 */
	TEST_MAX(((rule & 0x00030000000L) << 12) +
			 ((rule & 0x0C000000000L))       +
			 ((rule & 0x00000300000L) << 16) +
			 ((rule & 0x00000C00000L) << 12) +
			 ((rule & 0x00003000000L) << 8)  +
			 ((rule & 0x0000C000000L) << 4));
	
	/* flip + rotate 4 */
	TEST_MAX(((rule & 0x30000000000L))       +
			 ((rule & 0x000C0000000L) << 8)  +
			 ((rule & 0x00300000000L) << 4)  +
			 ((rule & 0x00C00000000L))       +
			 ((rule & 0x03000000000L) >> 4)  +
			 ((rule & 0x0C000000000L) >> 8));
	
	/* flip + rotate 5 */
	TEST_MAX(((rule & 0x00000000003L) << 40) +
			 ((rule & 0x0000000000CL) << 36) +
			 ((rule & 0x000000000C0L) << 30) +
			 ((rule & 0x00000003000L) << 22) +
			 ((rule & 0x00000300000L) << 12) +
			 ((rule & 0x000C0000000L)));
	
	return rule == max_equiv(rule);
}

int check_tile(uint64_t rule)
{
	int heesch = 0;
	size_t target = CORONA_LAST(1);
	size_t i = 1;
	int orient = -1;
	time_t timestart, timenow;
	time(&timestart);
	while (i < GRID_MAX && i) // either we've filled the grid or we backtracked to the beginning
	{
		time(&timenow);
		if ((timenow - timestart) > TILE_TIMEOUT)
		{
			return GRID_SIZE + 1;
		}
		
		if (try_place(grid, rule, i, orient) < 0) // placement failed, backtrack
		{
			orient = grid[--i];
		} else // placement success, onward
		{
		orient = -1;
			if (i++ == target) // we've filled a corona
			{
				++heesch;
				target = CORONA_LAST(heesch + 1);
			}
		}
	}
	return heesch;
}

int try_place(int *grid, uint64_t rule, size_t i, int orient)
{
	size_t ui, uli, dli, di, dri, uri;
	int up, upleft, downleft, down, downright, upright;
	int uside, ulside, dlside, dside, drside, urside;
	int16_t x, y;
	uint32_t coords = hex_coords(i);
	x = HEX_X(coords);
	y = HEX_Y(coords);

	ui = hex_index(x, y+1);
	uli = hex_index(x-1, y+1);
	uri = hex_index(x+1, y);
	dli = hex_index(x-1, y);
	dri = hex_index(x+1, y-1);
	di = hex_index(x, y-1);
	
	up = (ui > i) ? 0 : grid[ui];
	upleft = (uli > i) ? 0 : grid[uli];
	upright = (uri > i) ? 0 : grid[uri];
	downleft = (dli > i) ? 0 : grid[dli];
	downright = (dri > i) ? 0 : grid[dri];
	down = (di > i) ? 0 : grid[di];

	uside = (ui > i) ? -1 : TRANSFORM(3, up);
	ulside = (uli > i) ? -1 : TRANSFORM(2, upleft);
	urside = (uri > i) ? -1 : TRANSFORM(4, upright);
	dlside = (dli > i) ? -1 : TRANSFORM(1, downleft);
	drside = (dri > i) ? -1 : TRANSFORM(5, downright);
	dside = (di > i) ? -1 : TRANSFORM(0, down);
	
	int xform;
	for (xform = orient + 1; xform < MAX_XFORM; ++xform)
	{
		if (edge_match(rule, TRANSFORM(0,xform), uside, (xform ^ up) & 0x1) &&
			edge_match(rule, TRANSFORM(1,xform), urside, (xform ^ upright) & 0x1) &&
			edge_match(rule, TRANSFORM(2,xform), drside, (xform ^ downright) & 0x1) &&
			edge_match(rule, TRANSFORM(3,xform), dside, (xform ^ down) & 0x1) &&
			edge_match(rule, TRANSFORM(4,xform), dlside, (xform ^ downleft) & 0x1) &&
			edge_match(rule, TRANSFORM(5,xform), ulside, (xform ^ upleft) & 0x1))
		{
			grid[i] = xform;
			return xform;
		}
	}
	return -1;
}
